
<?php 
$prev = 445519;
$prevention_number = number_format($prev) ;
?>
<?php
include_once('components/Header.php');
?>


<link rel="stylesheet" href="assets/css/whatwedo.css"> 
<div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid #ff5757; text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158)">
            
                &nbsp&nbspPrevention Program &nbsp&nbsp
            
            </div>
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
                <span class="middloo3" style="text-align:center; justify-content:center;margin-top:20vh;font-size:23px ;">
                <p style="color:rgb(2, 36, 92)!important; text-align:center;">The aim of the Prevention Program is to develop the knowledge, know-how and necessary self-<br>protection skills needed by caregivers, professionals and children in order to prevent violence.<br><br><br>
                    Awareness activities are carefully prepared and delivered within public or private sectors,<br>
                    indirectly allowing facilitators to detect cases of violence while raising awareness on the subject.
                    
                    </p></span>
                    <div style="justify-content:center;text-align:center;padding-top: 70px;">
                    <div class="awalflex">
                        <img src="assets/icon/hand.svg"  > 
                        <p class="preven" style="font-family:Patrick Hand ;font-size:40px;font-weight:500 ;padding-top:10px ;">Since 2008 the Prevention Program has Reached</p>
                        
                        <?php echo "<p class='preven' style='font-family:Patrick Hand ;font-size:120px;font-weight:500 ;'>$prevention_number</p> 
                        <p class='preven' style='font-family:Patrick Hand ;font-size:40px;font-weight:500 ;margin-top:-250px ;padding-bottom: 40px;'>People</p>";?>
                        
                      </div></div>
                      <span class="middloo3" style="text-align:center; justify-content:center;margin-top:20vh;font-size:23px ;">
                      <p style="color:rgb(2, 36, 92)!important; text-align:center;">To know more about our statistics, kindly refer to this link:<br>Statistics reports<br><br><br>
                        If you work with children and would like to organize awareness sessions, please contact our<br>
                        prevention program at 79 300 413 or prevention@himaya.org
                        
                        </p></span>
                        <div  style="text-align:center;justify-content:center;padding-top:80px ; ">
                        <img src="assets/icon/line.svg" ></div>


                        <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
                            <div style="display:inline-block;vertical-align:top;">
                               <img src="assets/icon/heart.svg" alt="img"/>
                               </div>
                               <div style="display:inline-block;font-family:Patrick Hand ;font-size: 30px;color:rgb(21, 137, 158)">
                               
                                   &nbsp&nbspPrevention Program Gallery&nbsp&nbsp
                               
                               </div>
                               <div style="display:inline-block;vertical-align:top;">
                                <img src="assets/icon/heart.svg" alt="img"/>
                                </div></div>
                                <div class="prevengal" style="align-items:center; text-align:center;padding-bottom: 20vh;">
                                    <img src="assets/icon/preventgallery.png" width="60%">
                                </div>
                                
<?php include_once('components/Footer.php');?>

  </body>
</html>