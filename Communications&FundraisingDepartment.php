<?php
include_once('components/Header.php');

?>

<link rel="stylesheet" href="assets/css/whatwedo.css"> 
        <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid rgb(21, 137, 158); text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div class="prev" style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158);margin-top:-26px ;">
            
                &nbsp&nbspCommunications &<br>
                Fundraising Department&nbsp&nbsp&nbsp&nbsp
            
            </div>
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:23px;font-family: sans-serif;font-weight:lighter ;">
                        <p style="color:rgb(2, 36, 92); text-align:center;">&nbsp &nbsp &nbsp &nbsp  &nbsp In order to achieve social change, the Communication & Fundraising Department runs awareness<br>
                          campaigns and collaborates with influential figures to encourage people to "break the silence".<br><br>
                          The department has also built online communities spread out on several social media platforms,<br>
                          including Facebook, Twitter, Youtube and Instagram, in order to raise public awareness about the<br>
                          harsh reality of violence against children in Lebanon.<br><br>
                          The department aims to mediate between two main parties:<br><br>
                          &nbsp &nbsp &nbsp &nbsp  &nbsp The socially responsible individuals/corporations who aim to create a positive change in their<br>
                          society and himaya who works with vulnerable children victim of violence.
                        </div>
                  </div>
                   
                        <div  style="text-align:center;justify-content:center;padding-top:30px ; ">
                        <img class ="line"src="assets/icon/line.svg" ></div>


                        <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
                            <div style="display:inline-block;vertical-align:top;">
                               <img src="assets/icon/heart.svg" alt="img"/>
                               </div>
                               <div style="display:inline-block;font-family:Patrick Hand ;font-size: 30px;color:rgb(21, 137, 158)">
                               
                                   &nbsp&nbspCBRD Department Gallery&nbsp&nbsp
                               
                               </div>
                               <div style="display:inline-block;vertical-align:top;">
                                <img src="assets/icon/heart.svg" alt="img"/>
                                </div></div>
                                <div class="prevengal" style="align-items:center; text-align:center;padding-bottom: 20vh;">
                                    <img src="assets/icon/groupe.png" width="60%">
                                </div>
                                <?php 
include_once('components/Footer.php');
    ?>
 
  </body>
</html>