<?php
include_once('components/Header.php');?>

<link rel="stylesheet" href="assets/css/whatwedo.css"> 
        
        <div class="image">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div class="prev" style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158)">
            <p>
                &nbsp&nbspPrevention Program &nbsp&nbsp
            </p>
            </div>
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:23px;font-family: sans-serif;font-weight:lighter ;">
                        <p>The aim of the Prevention Program is to develop the knowledge, know-how and necessary self-<br>protection skills needed by caregivers, professionals and children in order to prevent violence.<br><br>
                          Awareness activities are carefully prepared and delivered within public or private sectors,<br>
                          indirectly allowing facilitators to detect cases of violence while raising awareness on the subject.
                      </div>
                  </div>
                    <div style="justify-content:center;text-align:center;padding-top: 70px;">
                    <div class="awalflex">
                        <img src="assets/icon/hand.svg"  > 
                        <p class="preven" style="font-family:Patrick Hand ;font-size:40px;font-weight:500 ;padding-top:10px ;">Since 2008 the Prevention Program has Reached</p>
                        
                        <p class="preven" style="font-family:Patrick Hand ;font-size:120px;font-weight:500 ;">445,519</p>
                        <p class="preven" style="font-family:Patrick Hand ;font-size:40px;font-weight:550 ;margin-top:-45px ;padding-bottom: 40px;">People</p>
                      </div></div>
                  
                        <div style="text-align: center;">
                          <div style="display: inline-block; text-align: left;margin-right:20vh;font-size:25px;font-weight: 400;">
                            <p>To know more about our statistics, kindly refer to this link:<br>Statistics reports<br><br>
                              If you work with children and would like to organize awareness sessions, please contact our<br>
                              prevention program at 79 300 413 or prevention@himaya.org.
                          </div>
                      </div>
                        <div  style="text-align:center;justify-content:center;padding-top:80px ; ">
                        <img src="assets/icon/line.svg" ></div>


                        <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
                            <div style="display:inline-block;vertical-align:top;">
                               <img src="assets/icon/heart.svg" alt="img"/>
                               </div>
                               <div style="display:inline-block;font-family:Patrick Hand ;font-size: 30px;color:rgb(21, 137, 158)">
                               <p>
                                   &nbsp&nbspPrevention Program Gallery&nbsp&nbsp
                               </p>
                               </div>
                               <div style="display:inline-block;vertical-align:top;">
                                <img src="assets/icon/heart.svg" alt="img"/>
                                </div></div>
                                <div class="prevengal" style="align-items:center; text-align:center;padding-bottom: 20vh;">
                                    <img src="assets/icon/preventgallery.png" width="60%">
                                </div>
                <?php include_once('components/Footer.php');?>
        
  </body>
</html>