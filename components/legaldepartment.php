<?php
include_once('components/Header.php');

?>
    <link rel="stylesheet" href="assets/css/whatwedo.css">    
        <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid #fbf75a; text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div class="prev" style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158);margin-top:-1px ;">
            
                &nbsp&nbspLegal Department
                &nbsp&nbsp
            
            </div>
            
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
                
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:23px;font-family: sans-serif;font-weight:lighter ; ">
                        <p style="color:rgb(2, 36, 92); text-align:center;">&nbsp&nbsp&nbsp&nbsp&nbsp Our legal department, in parallel with our other departments and units, serves as a support system<br>
                            for other programs; it coordinates communication between our other departments and the<br>
                            ministries when needed, and represents our organization when lobbying or campaigning for issues<br>
                            that require government involvement.<br><br>
                            &nbsp&nbsp&nbsp&nbsp&nbspThe department works hand in hand with different members of the team (mainly our social<br>
                            workers) to ensure that legal procedures are followed and respected; it also provides necessary<br>
                            legal training sessions for members of our organization. Our legal team intervenes if a child is<br>
                            found to be in danger in his/her own home or surroundings, and are in constant contact with law<br>
                            enforcement agencies. Our legal team is also responsible for making sure important files are being<br>
                            properly followed up on by the ministries.<br><br>
                            &nbsp&nbsp&nbsp&nbsp&nbspOur professionals are also part of the Parliamentary Committee fighting for the amendment of<br>
                            child protection laws in Lebanon; mainly law 422. They must conduct research on relevant child<br>
                            protection laws in Lebanon, including alternative care.<br>
                        </div>
                  </div>
                   
                       
                  <?php 
include_once('components/Footer.php');
    ?>
 
  </body>
</html>