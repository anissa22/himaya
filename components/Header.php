<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" 
      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
      <link href="https://fonts.googleapis.com/css2?family=Oswald&display=swap" rel="stylesheet">
      
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" rel="stylesheet">  
    <meta charset="UTF-8">
    <link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Patrick+Hand&display=swap" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://kit.fontawesome.com/95dc93da07.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;700;900&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" rel="stylesheet">
  <title>Himaya</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="icon" href="assets/icon/Himaya logo colored.svg">
  <link href="https://fonts.cdnfonts.com/css/gordita" rel="stylesheet">
  </head>
  
  <body222>
        <nav class="navbar navbar-expand-lg navbar-light  fixed-top" id="navbar" style="z-index: 10000;">
            <div class="container">
                <a class="navbar-brand" href="home.php">
                    <img style="height: 50px;;" class="karim" src="assets/icon/himaya logo white.svg">
                </a> 
                <button aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler" data-bs-target="#navbarSupportedContent" data-bs-toggle="collapse" type="button"style="background-color:white;">
                    <span class="navbar-toggler-icon" style="background-color:white;"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <div class="dropdown">
                                <button class="dropbtn">What We Do</button>
                                <div class="dropdown-content">
                                  <a href="preventionprogram.php">Prevention Program</a>
                                  <div class="row-border"></div>
                                  <a href="resilianceprog.php">Resilience Program</a>
                                  <div class="row-border"></div>
                                  <a href="Capacity Building.php">CBRD Department</a>
                                  <div class="row-border"></div>
                                  <a href="judical.php">Judicial Protection & Advocacy
                                    Department</a>
                                    <div class="row-border"></div>
                                  <a href="Communications&FundraisingDepartment.php">Communications & Fundraising
                                    Department</a>
                                    <div class="row-border"></div>
                                    
                                  <a href="QualityDepartment.php">Quality Department</a>
                                  <!-- <div class="row-border"></div>
                                  <a href="legaldepartment.php">Legal Department  </a> -->
                                  
                                </div>
                            </div>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link"  href="schema.php">Schema</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link"  href="tips2.php">Tips</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="Media.php">Media</a>
                        </li>
                        <li class="nav-item active">
                            <a href="donate.php">
                                <button class="btncarou">Donate</button>
                            </a>
                        </li>
                        <li class="nav-item active">
                            <div class="dropdown" >
                                <button class="dropbtn" style="margin-left:40px;padding-top:3px;">
                                <i class="fa fa-bars" aria-hidden="true"></i>
                                </button>
                                <div class="dropdown-content" style="min-width:80px ;width:180px;">
                                  <a class="a" href="meet.php">Meet the Family</a>
                                  <div class="row-border"></div>
                                  <a href="contactus.php">Contact Us</a>
                                  <div class="row-border"></div>
                                  <a href="commiteemembers.php">Meet the Board</a>
                                  <div class="row-border"></div>
                                  <a href="ourpartners.php">Our Sponsors</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

 
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js">
      </script> 
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js">
      </script> 
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js">
      </script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js">
      </script> 
      <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.3/dist/umd/popper.min.js">
      </script> 
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.min.js">
      </script>
     </body222>