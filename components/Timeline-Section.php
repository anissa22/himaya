<div class="ag-timeline_list">
            <div class="js-timeline_item ag-timeline_item">
              <div class="ag-timeline-card_box">
                <div class="js-timeline-card_point-box ag-timeline-card_point-box">
                  <div class="ag-timeline-card_point">1</div>
                </div>
                <div class="ag-timeline-card_meta-box">
                  <div class="ag-timeline-card_meta">الطفل نفسه</div>
                </div>
              </div>
              <div class="ag-timeline-card_item">
                <div class="ag-timeline-card_inner">
                  <div class="ag-timeline-card_img-box">
                    <img src="assets/images/Picture2.png" class="ag-timeline-card_img" width="640" height="500" />
                  </div>
                  <div class="ag-timeline-card_info">
                    <div class="ag-timeline-card_title">الطفل نفسه</div>
                    <!--<div class="ag-timeline-card_desc">
                    </div>-->
                  </div>
                </div>
                <div class="ag-timeline-card_arrow"></div>
              </div>
            </div>


            <div class="js-timeline_item ag-timeline_item">
              <div class="ag-timeline-card_box">
                <div class="ag-timeline-card_meta-box">
                  <div class="ag-timeline-card_meta">برنامج التوعية والوقاية</div>
                </div>
                <div class="js-timeline-card_point-box ag-timeline-card_point-box">
                  <div class="ag-timeline-card_point">2</div>
                </div>
              </div>
              <div class="ag-timeline-card_item">
                <div class="ag-timeline-card_inner">
                  <div class="ag-timeline-card_img-box">
                    <img src="assets/images/Picture3.png" class="ag-timeline-card_img" width="640" height="500" alt="" />
                  </div>
                  <div class="ag-timeline-card_info">
                    <div class="ag-timeline-card_title">برنامج التوعية والوقاية</div>
                    <!--<div class="ag-timeline-card_desc">
                      Donec pede justo, fringilla
                      vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                      venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer
                      tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend
                      tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.
                      Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
                      nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.
                    </div>-->
                  </div>
                </div>
                <div class="ag-timeline-card_arrow"></div>
              </div>
            </div>
  
            <div class="js-timeline_item ag-timeline_item">
              <div class="ag-timeline-card_box">
                <div class="js-timeline-card_point-box ag-timeline-card_point-box">
                  <div class="ag-timeline-card_point">3</div>
                </div>
                <div class="ag-timeline-card_meta-box">
                  <div class="ag-timeline-card_meta">البيئة الأسرية</div>
                </div>
              </div>
              <div class="ag-timeline-card_item">
                <div class="ag-timeline-card_inner">
                  <div class="ag-timeline-card_img-box">
                    <img src="assets/images/Picture4.png" class="ag-timeline-card_img" width="640" height="500" alt="" />
                  </div>
                  <div class="ag-timeline-card_info">
                    <div class="ag-timeline-card_title">البيئة الأسرية</div>
                    <!--<div class="ag-timeline-card_desc">
                      Aenean imperdiet. Etiam ultricies
                      nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.
                      Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet
                      adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar,
                      hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae
                      sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget
                      eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh.
                    </div>-->
                  </div>
                </div>
                <div class="ag-timeline-card_arrow"></div>
              </div>
            </div>
  
            <div class="js-timeline_item ag-timeline_item">
              <div class="ag-timeline-card_box">
                <div class="ag-timeline-card_meta-box">
                  <div class="ag-timeline-card_meta">قسم بناء القدرات والبحث والتطوير</div>
                </div>
                <div class="js-timeline-card_point-box ag-timeline-card_point-box">
                  <div class="ag-timeline-card_point">4</div>
                </div>
              </div>
              <div class="ag-timeline-card_item">
                <div class="ag-timeline-card_inner">
                  <div class="ag-timeline-card_img-box">
                    <img src="assets/images/Picture5.png" class="ag-timeline-card_img" width="640" height="500" alt="" />
                  </div>
                  <div class="ag-timeline-card_info">
                    <div class="ag-timeline-card_title">قسم بناء القدرات والبحث والتطوير</div>
                    <!--<div class="ag-timeline-card_desc">
                      Cum sociis natoque penatibus et magnis dis parturient
                      montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
                      pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla
                      vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                      venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer
                      tincidunt. Cras dapibus. Vivamus elementum semper nisi.
                    </div>-->
                  </div>
                </div>
                <div class="ag-timeline-card_arrow"></div>
              </div>
            </div>
  
            <div class="js-timeline_item ag-timeline_item">
              <div class="ag-timeline-card_box">
                <div class="js-timeline-card_point-box ag-timeline-card_point-box">
                  <div class="ag-timeline-card_point">5</div>
                </div>
                <div class="ag-timeline-card_meta-box">
                  <div class="ag-timeline-card_meta">المجتمع المحلي والمؤسسات</div>
                </div>
              </div>
              <div class="ag-timeline-card_item">
                <div class="ag-timeline-card_inner">
                  <div class="ag-timeline-card_img-box">
                    <img src="assets/images/Picture6.png" class="ag-timeline-card_img" width="640" height="500" alt="" />
                  </div>
                  <div class="ag-timeline-card_info">
                    <div class="ag-timeline-card_title">المجتمع المحلي والمؤسسات</div>
                   <!-- <div class="ag-timeline-card_desc">
                      Vivamus elementum semper nisi. Aenean vulputate eleifend
                      tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.
                      Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra
                      nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies
                      nisi vel augue. Curabitur ullamcorper ultricies nisi.
                    </div>-->
                  </div>
                </div>
                <div class="ag-timeline-card_arrow"></div>
              </div>
            </div>
  
          
            
  
  
          </div>