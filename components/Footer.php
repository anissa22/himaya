            
<footer class="footer-distributed">

<div class="footer-left">

  <h3 style="font-family: 'Gordita', sans-serif;font-size: 45px;color: rgb(72, 72, 72);">Follow Us</h3>
  <div>
  <img class="iconfooter" src="assets/icon/twitterblue.svg" style="width:10%;height:10%;">
  <img class="iconfooter" src="assets/icon/instablue.svg" style="width:10%;height:10%;">
  <img class="iconfooter" src="assets/icon/fbblue.svg" style="width:10%;height:10%;">
  </div><br>
  
  

  <p class="footer-company-name">Copyright © 2022 himaya. All rights reserved</p>
</div>

<div class="footer-center">

  <div>
   <img class="iconfooter" src="assets/icon/location.svg">
    <p>&nbsp&nbsp &nbsp&nbsp ST. RITA BUILDING, STREET 58, 1ST FLOOR, FANAR, LEBANON</p>
  </div>

  <div>
    <img class="iconfooter" src="assets/icon/phone.svg">
    <p>&nbsp&nbsp &nbsp &nbsp (961) 1 395 315/ 6/ 7/ 8/ 9</p>
  </div>

  <div>
    <img class="iconfooter" src="assets/icon/emailblack.svg">
    <p><a href="mailto:INFO@HIMAYA.ORG">&nbsp &nbsp &nbsp &nbspINFO@HIMAYA.ORG</a></p>
  </div>

</div>

<div class="footer-right">

  <img class="himayafooter" src="assets/icon/Himaya logo colored.svg">

</div>


</footer>
<img class="imgfo" src="assets/icon/footer.svg">
          
</body>
<script>
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {

document.getElementById("navbar").style.background = "rgb(21, 137, 158)";
} else {

document.getElementById("navbar").style.background = "none";
}
}
</script>
 <!-- jaquery link -->

  <script src="assets/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->

<!--modernizr.min.js-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>


<!--bootstrap.min.js-->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="assets/js/bootsnav.js"></script>

<!-- for manu -->
<script src="assets/js/jquery.hc-sticky.min.js" type="text/javascript"></script>


<!-- vedio player js -->
<script src="assets/js/jquery.magnific-popup.min.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

<!--owl.carousel.js-->
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>

<!-- counter js -->
<script src="assets/js/jquery.counterup.min.js"></script>
<script src="assets/js/waypoints.min.js"></script>

<!--Custom JS-->
<script type="text/javascript" src="assets/js/jak-menusearch.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>

</script>
