<style>
<?php include ('assets/css/whatwedo.css'); ?>
</style>        

<?php
include_once('components/Header.php');

?><div class="image" style=" background-image: url(assets/icon/report.png); text-align: center;border: none;display: block; height: 550px;filter: brightness(80%);min-width: 100%; width: 100%; min-width: 100%;">
<h2 class="heading3">Media</h2>
</div>

<main style="padding-top: 40px;">

<ul id="paginated-list" data-current-page="1" aria-live="polite">
 <item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/videoseries?list=PLjgsR0MgHHPqhPwfu_wHaevake3VWso3Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Especial NTN24 | Lanzan proyecto de capoeira para niños refugiados en el Líbano</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 23/10/2016</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"> <a href="https://youtu.be/pflgyJeNTY8">Click here to watch the video on Youtube</a> </p>
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/w_a_pnNqxfE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Michel Maragel, psychologue, explique le difficile quotidien des réfugiés syriens au Liban</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 12/10/2016</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"> <a href="https://youtu.be/w_a_pnNqxfE">Click here to watch the video on Youtube</a> </p>
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/pz9-jko6UfI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on OTV</p><br><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 22/12/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"> <a href="https://youtu.be/pz9-jko6UfI">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
<div class="container-fluid py-5">
<div class="container">
   
    <div class="row g-5">
        <div class="col-xl-4 col-lg-4" >
            <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
            <iframe width="380" height="315" src="https://www.youtube.com/embed/GEKPEWe6YcE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="p-4">
                    <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Team Leader in the North on NBN</p><br>
                    <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 22/12/2020</p><br>
                    <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"> <a href="https://youtu.be/GEKPEWe6YcE">Click here to watch video on Youtube</a> </p>
                  
                </div>
              
            </div>
        </div>
        <div class="col-xl-4 col-lg-4">
            <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
            <iframe width="380" height="315" src="https://www.youtube.com/embed/oQ4MvVvaiiE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="p-4">
                    <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on Sawt El Mada</p><br>
                    <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 22/12/2020</p><br>
                    <p class="card-text" style="float:left ;font-size:20;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/oQ4MvVvaiiE">Click here to watch video on Youtube</a> </p>
                  
                </div>
              
            </div>
        </div>
        <div class="col-xl-4 col-lg-4">
            <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
            <iframe width="380" height="315" src="https://www.youtube.com/embed/BpXH57NHPek" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <div class="p-4">
                    <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on MTV alive</p><br>
                    <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 18/12/2020</p><br>
                    <p class="card-text" style="float:left ;font-size:20px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/BpXH57NHPek">Click here to watch video on Youtube</a></p>
                </div>
              
            </div>
        </div>
    </div>
</div>
</div></item>
<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/jdU668adfr0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on MTV</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 7/11/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:20px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/jdU668adfr0">Click here to watch video on Youtube</a></p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/C9UDQLZSlho" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview on MTV on Cyber Abuse</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 23/11/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:20px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/C9UDQLZSlho">Click here to watch video on Youtube</a></p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/hr0gEbmjscU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with our clinical psychologist Rouba Nahas on Time Management on MTV Alive</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 28/4/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:20px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/hr0gEbmjscU">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/R-dIev6RzYw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on يوم جديد | OTV</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 28/4/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:20px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left"><a href="https://youtu.be/R-dIev6RzYw">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/h40UyjWf31I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with one of himaya's team leaders on Maryam TV</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 28/4/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left"><a href="https://youtu.be/h40UyjWf31I">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/8cPx66YlyLI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Community Mobilizer in Central and West Bekaa Sarah Ghazal on Al Araby TV</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 28/4/2020</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/8cPx66YlyLI">Click here to video on Youtube</a> </p>
                    
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/hQE6q7OMBVA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on OTV</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 19/3/2019</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/hQE6q7OMBVA">Click here to watch video on Youtube</a></p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/AMnLgL0gAmM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with himaya's Executive Director on احلى صباح show | Tele Liban</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 19/3/2019</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/AMnLgL0gAmM">Click here to wathc video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/PtqnGhnJtm0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Future TV interview on Dawwi 3al Ghalat campaign during عالم الصباح show</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 19/3/2019</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/PtqnGhnJtm0">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
<item><<div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/-WkphFUwYc8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Executive Director Lama Yazbeck speaking about our 3rd Annual Symposium on MTV ALIVE.</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 29/9/2017</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left"><a href="https://youtu.be/-WkphFUwYc8">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/2ehNVFQ5-4Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">عالم الصباح فقرة خاصة مع لمى يزبك</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 3/7/2017</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/2ehNVFQ5-4Y">Click here t watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/zVUhvYv318Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Bte7la El Hayet Episode 304 السيدة لمى يزبك</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 3/7/2017</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/zVUhvYv318Y">Click here to watch video on Youtube</a></p>
                   
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/yMl3p1flU04" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Interview with Noursat on Internet Safety</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 17/3/2021</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/yMl3p1flU04">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/Df0pfPsU9JI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">عالموعد - مع باتريسيا الخوري حول وضع الأطفال في ظلّ الأزمات الإقتصاديّة، الماليّة والسّياسيّة</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 7/7/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/Df0pfPsU9JI">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/efaPnjQQhZU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">Himaya NGO at LIFF Lebanese Independent Film Festival 2022</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 7/9/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/efaPnjQQhZU">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/sV1x70cnMrw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">himaya on "7ki Jaless" LBC</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 28/1/2015</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/sV1x70cnMrw">Click here to watch video on Youtube</a> </p>

                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/b6Nxqg26meA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">3 cases of abuse followed up on by himaya - Hani Assaf (2014)</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 3/11/2014</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/b6Nxqg26meA">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
              <iframe width="380" height="315" src="https://www.youtube.com/embed/1vBuXN3EUIw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:20px;display: inline-block;padding:0;">About himaya - By Carole Mansour (2013)</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 31/10/2014</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 0px 0px ;text-align: left"><a href="https://youtu.be/1vBuXN3EUIw">Click here to watch video on Youtube</a> </p>
                     
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>

<!--<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                  <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                     <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                  <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                     <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                  <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                     <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item>
              -->
<!--<item><div class="container-fluid py-5">
  <div class="container">
     
      <div class="row g-5">
          <div class="col-xl-4 col-lg-4" >
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                  <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                     <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                  <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                     <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                  </div>
                
              </div>
          </div>
          <div class="col-xl-4 col-lg-4">
              <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                  <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                  <div class="p-4">
                      <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                      <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                      <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                     <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                  </div>
                
              </div>
          </div>
      </div>
  </div>
</div></item> -->
   <!-- <item><div class="container-fluid py-5">
      <div class="container">
         
          <div class="row g-5">
              <div class="col-xl-4 col-lg-4" >
                  <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                      <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                      <div class="p-4">
                          <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                          <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                          <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                         <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                      </div>
                    
                  </div>
              </div>
              <div class="col-xl-4 col-lg-4">
                  <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                      <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                      <div class="p-4">
                          <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                          <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                          <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                         <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                      </div>
                    
                  </div>
              </div>
              <div class="col-xl-4 col-lg-4">
                  <div class="bg-light rounded overflow-hidden" style="border-bottom:17px solid #fbd75a ;">
                      <img class="img-fluid w-100" src="icon/kid-boy-child-childhood.jpg" alt="">
                      <div class="p-4">
                          <p class="card-titleme" style="float:left;margin-top:1.5vw;font-family:Patrick Hand;font-size:40px;display: inline-block;padding:0;line-height:0.2;">News Title</p><br>
                          <p style="font-size:20px ;display:block;margin-top:-25px ;margin-right:15vw ;"> 06/09/2022</p><br>
                          <p class="card-text" style="float:left ;font-size:19px;width: auto;margin-top:-3vw ;padding:0px 20px 0px ;text-align: left">This is a longer card with supporting text below as a natural lead-in to additional content. This content is a little bit longer. </p>
                         <a style="color: rgb(228, 73, 99);cursor: pointer;float: left;font-size: 19px;padding-bottom:24px ;">Read more</a>
                      </div>
                    
                  </div>
              </div>
          </div>
      </div>
  </div></item>-->


</ul>

    
                
                        
<?php include_once('components/Pagination.php');?> 
          </main>
          
          
                
              
<?php include_once('components/Footer.php');  ?>
        
 
  </body>
</html>