<?php
include_once('components/Header.php');
?>
<link rel="stylesheet" href="assets/css/whatwedo.css"> 
        <div class="image" style=" background-image: url(assets/icon/report.png);border: none; text-align: center;display: block; height: 550px;filter: brightness(80%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">Report a Case</h1>
         </div>
         <!-- body-->
         
                
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:23px;font-family: sans-serif;font-weight:lighter ;padding-top: 70px;">
                        <span style="color: rgb(21, 137, 158);font-size: 35px;font-family: Patrick hand;"><u>You Can Report a Case of Child Abuse Through</u></span><br>
► The hotline 03 414 964<br>
► E-mail resilience@himaya.org<br>
► E-helpline on the website's main page<br>
► In-person by visiting our offices<br>
► Facebook or Instagram @himayaleb<br>
► Dial #HIMAYA<br><br>
<span style="color: rgb(21, 137, 158);font-size: 35px;font-family: Patrick hand;"><u>What Happens After You Report a Case of Child Abuse?</u></span><br>

► After receiving a case, a social worker reviews it then refers it to an appropriate Senior Case Manager (SCM)<br>
based on the region in which the case occurred.<br>
► The SCM then assigns the given case to a social worker and a psychologist who then work together on the<br>
case’s assessment and intervention.<br><br>
<span style="color: rgb(21, 137, 158);font-size: 35px;font-family: Patrick hand;"><u>How Does It Work?</u></span><br>

► The assigned social worker and psychologist organize individual meetings with the child and parents at<br>
Himaya’s offices to assess protection risks and start establishing a partnership with the family. The social<br>
worker then conducts further home visits, to assess the environment that the family is living in.<br>
► If the abuse is of high risk and is life-threatening to the child, the situation requires immediate intervention.<br>
In this case, the assessment goes through a judicial pathway, where general prosecutors or juvenile judges<br>
ensure the child’s protection and stop the perpetrator.<br>
► When abuse is not of high risk and parents are cooperative, this primary assessment may take up to 14<br>
days.<br>
► If the outcome assessment report delivered by the social worker and psychologist shows no signs of child<br>
abuse (whether physical, psychological, sexual or neglect), then the case gets closed with restitution to the<br>
child and family. If support other than protection is needed, the family and child get referred to other services.<br>
► If the outcome assessment report shows signs of abuse, partnership with the family is set to further assess<br>
the risk factors, the vulnerabilities of the child, the parents’ capabilities, and the resources within the<br>
environment to ensure proper intervention.<br><br>
► Each intervention is tailored to answer the needs of the child and family through specific objectives and<br>
adequate means, set within an agreed-upon timeframe.<br>
► The goals of each intervention are revised with the family every 3 months to evaluate its effectiveness and<br>
accommodate and plan eventually safe closure of the case.<br>
► After a case is closed, children and parents can resort to Himaya at any moment, when they feel that<br>
support is needed.<br>
                        </div>
                  </div>
                   
<?php 
include_once('components/Footer.php');
    ?>

        
  </body>
</html>