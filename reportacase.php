


<?php
include_once('components/Header.php');

?>

<link rel="stylesheet" href="assets/css/whatwedo.css"> 

    <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid rgb(21, 137, 158); text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
    <h1 class="heading2">What We Do</h1>
    </div>
                
    <div id="content" style=" height: 90%; width: 80%; margin:auto; margin-bottom:90%;margin-top:5%; align-items:center;">
        <div id="div1" style="float:left; height:55%;  width:50%;  margin-top:1.5%; margin-right:1%; background: rgb(1, 95, 112); align-items:center; padding:2.5%;">
            <span style="color: white;font-size: 25px;font-family: Baskerville Old Face; text-align:center;"><u>You Can Report a Case of Child Abuse Through</u></span><br><br>
                <span style="color: white;">► The hotline 03 414 964</span><br>
                <span style="color: white;">► E-mail resilience@himaya.org</span><br>
                <span style="color: white;">► E-helpline on the website's main page</span><br>
                <span style="color: white;">► In-person by visiting our offices</span><br>
                <span style="color: white;">► Facebook or Instagram @himayaleb</span><br>
                <span style="color: white;">► Dial #HIMAYA<br><br>
        </div>
        <div id="div2" style="float:left; height:50%;  width:48.8%; margin-bottom:1%; background: rgb(1, 95, 112); align-items:center; padding:2.5%;">
            <span style="color: white;font-size: 25px;font-family: Baskerville Old Face; text-align:center;"><u>What Happens After You Report a Case of Child Abuse?</u></span><br><br>

            <span style="color: white;">► After receiving a case, a social worker reviews it then refers it to an appropriate Senior Case Manager (SCM)<br>
            based on the region in which the case occurred.</span><br>
            <span style="color: white;">► The SCM then assigns the given case to a social worker and a psychologist who then work together on the<br>
            case’s assessment and intervention.</span><br><br>

        </div>
        
        <div id="div3" style="float:right;  height:90%; width:100%; background: rgb(1, 95, 112); align-items:center; padding:3.5%;">
            <span style="color: white;font-size: 25px;font-family: Baskerville Old Face; text-align:center;"><u>How Does It Work?</u></span><br><br>
            <span style="color: white;">► The assigned social worker and psychologist organize individual meetings with the child and parents at<br>
            Himaya’s offices to assess protection risks and start establishing a partnership with the family. The social<br>
            worker then conducts further home visits, to assess the environment that the family is living in.</span><br>
            <br><span style="color: white;">► If the abuse is of high risk and is life-threatening to the child, the situation requires immediate intervention.<br>
            In this case, the assessment goes through a judicial pathway, where general prosecutors or juvenile judges<br>
            ensure the child’s protection and stop the perpetrator.</span><br>
            <br><span style="color: white;">► When abuse is not of high risk and parents are cooperative, this primary assessment may take up to 14<br>
            days.<br>
            <br><span style="color: white;">► If the outcome assessment report delivered by the social worker and psychologist shows no signs of child<br>
            abuse (whether physical, psychological, sexual or neglect), then the case gets closed with restitution to the<br>
            child and family. If support other than protection is needed, the family and child get referred to other services.</span><br>
            <br><span style="color: white;">► If the outcome assessment report shows signs of abuse, partnership with the family is set to further assess<br>
            the risk factors, the vulnerabilities of the child, the parents’ capabilities, and the resources within the<br>
            environment to ensure proper intervention.</span><br><br>
            <br><span style="color: white;">► Each intervention is tailored to answer the needs of the child and family through specific objectives and<br>
            adequate means, set within an agreed-upon timeframe.</span><br>
            <br><span style="color: white;">► The goals of each intervention are revised with the family every 3 months to evaluate its effectiveness and<br>
            accommodate and plan eventually safe closure of the case.</span><br>
            <br><span style="color: white;">► After a case is closed, children and parents can resort to Himaya at any moment, when they feel that<br>
            support is needed.</span><br>

        </div>
                        
    </div>
        
    
</body>
<?php 
    include_once('components/Footer.php');
?>

<html>

