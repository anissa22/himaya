<?php
include_once('components/Header.php');

?>
<?php 
$phones = array('79 300 419','79 300 410', '76 450 753','71 802 883','03 414 964');
$locations = array('North Bekaa', 'North Lebanon', 'South Lebanon','Central and West Bekaa','Beirut and Mount Lebanon'  ); 
?>
   <link rel="stylesheet" href="assets/css/whatwedo.css">    
        <div class="image" style=" background-image: url(assets/icon/shakehand.png); text-align: center;border: none;display: block; height: 550px;filter: brightness(80%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">About Us</h1>
         </div>
         <!-- body-->
         <span class="middlooo">Child abuse is a critical issue that has long been neglected in Lebanon.</span>
        <img class="guysbeside" src="assets/icon/guysbeside.svg" style="max-width:85% ;margin-left: 9vh;">
        <span class="middlooo" style="height:15vh ;">&nbsp 1 in every 6 children<br>
          has suffered from abuse<br><br></span>
          <img src="assets/icon/losangestar.svg" class="khtefe" style="position:absolute;right: 89%;width:14%; top: 105%;">
           
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-family: sans-serif;font-weight: 100;">
                        <p style="font-size:28px ;"> This refers to only 1 out of 4 types of abuse a child can be exposed to: Physical<br>
                            Abuse, Psychological Abuse, Sexual Abuse, and Neglect.<br><br>
                            </p><p style="font-size:23px ;">
                            In light of these alarming results, himaya was founded in 2008. The organization has continued to<br>
grow, responding to child protection needs on a national level. The dedicated & multidisciplinary<br>
team of professionals covers all Lebanese territory with offices in Mount Lebanon, South Lebanon,<br>
North Lebanon, and the Bekaa; ensuring accessible services to children across Lebanon.<br><br>
In order to achieve its mission, himaya works with children as well as their families and the<br>
environment as a whole. himaya strives for a radical change on a national level in order to improve<br>
the lives of children in Lebanon.</p><br><br>
                        </div>
                  </div>
                  <!-- divs-->
                  <section class="section" style=" width: 120%;margin:0 ; padding: 0;" id="about">
                    <div class="container">
                        <div class="row " >
                            <div class="col-md-6 pr-md-5 sm-6 mb-4 mb-md-0 col-sm-4">
                                <h1 class="section-title mb-0" style="font-family: Patrick Hand;color: rgb(21, 137, 158);">Our Locations</h>
                                
                                <img src="assets/icon/location (2).svg" style="block" alt="" class="w-100 w-sm-40 mt-3 ">
                            </div>
                            <div class="col-md-6 pl-md-6 col-sm-6" style="margin: 0;padding:0">
                               
                                    <div class="col-6" style="margin-bottom: 30px;line-height:1.2 ;">
                                        <p style="font-family:patrick hand ;font-size:38px;color:rgb(21, 137, 158);"> Our Hotlines</p>
                                        <span style="font-family:open-sans ; font-size: 30px;">
                                        <?php 
                                        for ($i=0; $i<count($locations);$i++){
                                          echo "<span>$locations[$i]</span><br>";
                                          echo "<span style='font-family:Kanit ;font-weight: 600;'> $phones[$i]</span><br>";
                                        }
                                        ?>  
                                      </span></span>
                                </div>
                            </div>
                        </div>              
                    </div>
                </section>
                <img src="assets/icon/boatstars.svg" class="khtefe" style="position: absolute;left:84%;width:20%;top:190% ;">
                <div style="text-align: center;">
                    <div style="display: inline-block; text-align: left;margin-right:2vh;font-family: sans-serif;font-weight: 100;margin-top: 50px;">
                      <p style="font-size:28px ;">In December 2014, Himaya won an award from stars foundation UK & was<br>
                        awarded as a runner up for the protection sector in the Africa-Middle east region.<br><br>
                          </p><p style="font-size:23px ;">
                            The Stars Impact Awards recognize and reward effective, well-managed local organizations<br>
                            working to improve child health, education, protection and WASH (water, sanitation and hygiene)<br>
                            in the countries with the highest rates of under-five mortality.<br>
                            Today, himaya is a specialized NGO in the child protection sector, working to prevent violence<br>
                            against children and offer the support needed for abused children on a psychosocial and legal<br>
                            level; eventually allowing them to be reintegrated as productive and active members of society.<br>
                             In 2018, himaya became a member of the ISS (International Social Service) responding to cross<br>
                            border child abuse cases involving a Lebanese and a foreigner parent. himaya has also become<br>
                            mandated by the Ministry of Justice to handle all legal protection cases in the North Governorate.</p><br><br>
                      </div>
                </div>
                
                   
                                    
                              
<?php 
include_once('components/Footer.php');
    ?>

  </body>
</html>