
<style>
<?php include ('assets/css/Schema.css'); ?>
</style>        

<?php
include_once('components/Header.php');?>

     
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-124048250-1');
    </script>
    
    <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom:none; text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;padding-bottom: 40px;">
        <h3 class="heading2">Child Protection Network Scheme</h3>
    </div>


    <br>
    <br>
    
    <div>
        <section class="page-wrapper" >

        <div class="slider">
        
            <ul class="slider-list">
                
                <li class="slider-list__item  ">
                <span class="back__element">
                    <img src="assets/images/Himaya website-01.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-01.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-01.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-02.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-02.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-02.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-03.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-03.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-03.jpg" />
                </span>
                </li>
                
                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-04.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-04.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-04.jpg" />
                </span>
                </li>
                
                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-05.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-05.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-05.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-06.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-06.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-06.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-07.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-07.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-07.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-08.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-08.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-08.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-09.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-09.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-09.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-10.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-10.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-10.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-11.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-11.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-11.jpg" />
                </span>
                </li>

                <li class="slider-list__item">
                <span class="back__element">
                    <img src="assets/images/Himaya website-12.jpg" />
                </span>
                <span class="main__element">
                    <img src="assets/images/Himaya website-12.jpg" />
                </span>
                <span class="front__element">
                    <img src="assets/images/Himaya website-12.jpg" />
                </span>
                </li>
            </ul>
            
        <div class="slider__nav-bar">
            <a class="nav-control"></a>
            <a class="nav-control"></a>
            <a class="nav-control"></a>
            <a class="nav-control"></a>
            <a class="nav-control"></a>
        </div>
        
        <div class="slider__controls">
            <a class="slider__arrow slider__arrow_prev"></a>
            <a class="slider__arrow slider__arrow_next"></a>
        </div>

        </div>      

        </section>
    </div>
    
  
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.2.0/anime.min.js"></script>
    
    <script src="assets/js/schema.js"></script>

    
        <?php include_once('components/Footer.php');?>
  </body>
</html>