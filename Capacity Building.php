


<?php 
$cbrd = 1569;
$cbrd_program = number_format($cbrd);

?>
<?php
include_once('components/Header.php');

?>
 <link rel="stylesheet" href="assets/css/whatwedo.css"> 
 <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid rgb(21, 137, 158); text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div class="prev" style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158);margin-top:-26px ;">
            
                &nbsp&nbspCapacity Building, Research<br>
                &nbsp&nbsp & Development Department
            
            </div>
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:23px;font-family: sans-serif;font-weight:lighter ;">
                        <p style="color:rgb(2, 36, 92); text-align:center;">The Capacity Building and Research & Development Department (CBRD) strives to develop the<br>
                            skills of professionals working with children by providing them with specialized training sessions<br>and workshops related to child protection.<br><br>
                            CBRD organizes regular training sessions with Himaya's teams, as well as external training sessions<br>
                            and workshops for professionals working in the child protection sector.
                        </div>
                  </div>
                    <div style="justify-content:center;text-align:center;padding-top: 70px;">
                    <div class="awalflex">
                        <img src="assets/icon/open hand.svg"  > 
                        <p class="preven" style="font-family:Patrick Hand ;font-size:40px;font-weight:500 ;padding-top:10px ;">Since its creation in 2016, CBRD has trained</p>
                        
                        <?php echo"<p class='preven' style='font-family:Patrick Hand ;font-size:120px;font-weight:500 ;'>$cbrd_program</p>"; ?>
                        <p class="preven" style="font-family:Patrick Hand ;font-size:40px;font-weight:550 ;margin-top:-250px ;padding-bottom: 40px;">professionals</p>
                      </div></div>
                      <div style="text-align:center;font-family: Patrick Hand;font-size:45px;margin-top:-100px ;">in local and international NGOs, nurseries, schools, universities, as<br>well as Municipal Police and Internal Security Forces.
                      </div>
                  
                        <div style="text-align: center;">
                          <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:25px;font-weight: 400;margin-top: 8vh;">
                            <p style="color:rgb(2, 36, 92); text-align:center;">CBRD also works on strengthening the capacity of organizations to respond to child protection<br>
                                concerns by developing and implementing Child Protection Policies. So far, CBRD has<br>
                                implemented more than 10 CPP in schools and organizations across Lebanon, and is actively<br>
                                working with the Ministry of Public Health and UNICEF on strengthening child protection<br>
                                practices in the healthcare sector in Lebanon.<br><br>
                                Additionally, CBRD aims to align himaya's programs with best practices, as well as innovative and<br>
                                research-based approaches, and to contribute to advancing research in the child protection sector<br>
                                in Lebanon.<br><br>
                                Learn more about the Child Protection Policies: <a href="https://drive.google.com/file/d/167-YSdH2-<br>
                                IrT59RQjUryoF8Oh5y0_I1L/view?usp=sharing">Learn more</a><br>
                                Research Request form: <a href=" https://goo.gl/forms/VK4sKRzUyHpbmst22">Research Request form</a><br>
                                Training request form: <a href="https://goo.gl/forms/DtAZFQRrznGOwtJC2">Training request form</a><br>
                                <br>
                                For further information contact us:<br>
                                cbrd@himaya.org or 71/702316
                          </div>
                      </div>
                        <div  style="text-align:center;justify-content:center;padding-top:80px ; ">
                        <img src="assets/icon/line.svg" ></div>


                        <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
                            <div style="display:inline-block;vertical-align:top;">
                               <img src="assets/icon/heart.svg" alt="img"/>
                               </div>
                               <div style="display:inline-block;font-family:Patrick Hand ;font-size: 30px;color:rgb(21, 137, 158)">
                               
                                   &nbsp&nbspCBRD Department Gallery&nbsp&nbsp
                              
                               </div>
                               <div style="display:inline-block;vertical-align:top;">
                                <img src="assets/icon/heart.svg" alt="img"/>
                                </div></div>
                                <div class="prevengal" style="align-items:center; text-align:center;padding-bottom: 20vh;">
                                    <img src="assets/icon/darak.png" width="60%">
                                </div>
                              
<?php 
include_once('components/Footer.php');
    ?>
 
  </body>
</html>