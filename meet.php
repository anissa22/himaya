
<style>
<?php include ('assets/css/style1.css'); ?>
</style>        

<?php
include_once('components/Header.php');

?>
<style>
    /* Dropdown Button */
.dropbtn1 {
  background-color: rgb(208, 185, 91);
  color: black;
  padding: 16px;
  font-size: 16px;
  border: none;
  cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn1:hover, .dropbtn1:focus {
  background-color: rgb(258, 185, 91);
}

/* The container <div> - needed to position the dropdown content */
.dropdown1 {
  position: relative;
  display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content1 {
  display: none;
  position: absolute;
  background-color: #f1f1f1;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content1 button {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content1 a:hover {background-color: #ddd;}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}
</style>    
<div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom:none; text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;padding-bottom: 40px;">
<h3 class="heading2">Meet The Family</h3>
</div>  
             
<section class="section" style="padding-top: 90px">
    <div style="align-items: center;">
        <div class="tab-content">
            <div class="tab-pane fade show active" >
                <div style="align-items: center;margin-left:14vh;">
                    <div class="" >
                        <div class="row"  >
                            <div class="col-12 text-center mb-2">
                                <!-- Images Board -->
                                <?php include('components/Meet-Images-Board.php') ;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>   

<?php 
    include_once('components/Footer.php');
?>


<script>
/*    $('.containo').isotope({
      itemSelector: '.item',
      layoutMode: 'fitRows'
    });

    $('.portfolio-item').isotope({
      $('.tabs-container ul li').click(function(){
        $('.tabs-container ul li').removeClass('active');
        $(this).addClass('active');

        var selector = $(this).attr('data-filter');
        $('.containo').isotope({
          filter: selector
        });
        return false;
      })
    });
*/
</script>

<script>
/*
$('button[name="adminstration"]').click(function(){
 // $('button[name="adminstration"]').css("background", "rgb(252, 86, 3)");
  //$('button[name!="adminstration"]').css("background", "rgb(208, 185, 91)");
  
  $("#category-span").html("Adminstration");
});


$('button[name="prevention"]').click(function(){
  //$('button[name="prevention"]').css("background", "rgb(252, 86, 3)");
  //$('button[name!="prevention"]').css("background", "rgb(208, 185, 91)");
  $("#category-span").html("Prevention");
});

$('button[name="resillience"]').click(function(){
  //$('button[name="resillience"]').css("background", "rgb(252, 86, 3)");
  //$('button[name!="resillience"]').css("background", "rgb(208, 185, 91)");
  $("#category-span").html("Resillience");
});
*/

//change color of clicked button in dropdown menu containing categories of images
$('button').click(function(event){
    if(this.className == "btn btn-primary mx-1 text-dark"){
        //alert(this.className);
        var name = this.name;
        var category = $( this ).html();;
        $('button[name="'+name+'"]').css("background", "rgb(252, 86, 3)");  
        $('button[type="button"][name!="'+name+'"]').css("background", "rgb(208, 185, 91)");
        $("#category-span").html(category);

    }
});

/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown1").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn1')) {
    var dropdowns = document.getElementsByClassName("dropdown-content1");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <!-- isotope plugin -->
    <script src="https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js"></script>

    <script>
        // init Isotope
var $grid = $('#product-list').isotope({
  // options
});
// filter items on button click
$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = $(this).attr('data-filter');
  $grid.isotope({ filter: filterValue });
});


    </script>
<script>
                                    $(function(){
   // See if this is a touch device
   if ('ontouchstart' in window)
   {
      // Set the correct [touchscreen] body class
      $('html').removeClass('no-touch').addClass('touch');
     
      // Add the touch toggle to show text when tapped
      $('.gallery img').click(function(){
         $(this).closest('li').toggleClass('touchFocus');
      });
   }
});
                                </script>
        
  </body>
</html>