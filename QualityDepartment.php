<?php
include_once('components/Header.php');
?>
<link rel="stylesheet" href="assets/css/whatwedo.css"> 
        <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid #ff5757; text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div class="prev" style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158);margin-top:-1px ;">
            
                &nbsp&nbspQuality Department&nbsp&nbsp
            
            </div>
            
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
                <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
                    <div style="display:inline-block;vertical-align:top;">
                       <img src="assets/icon/heart.svg" alt="img"/>
                       </div>
                       <div style="display:inline-block;font-family:Patrick Hand ;font-size: 30px;color:rgb(21, 137, 158)">
                       
                           &nbsp&nbspMonitoring & Evaluation&nbsp&nbsp
                      
                       </div>
                       <div style="display:inline-block;vertical-align:top;">
                        <img src="assets/icon/heart.svg" alt="img"/>
                        </div></div>
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:27px;font-family: sans-serif;font-weight:lighter ;">
                        <p style="color:rgb(2, 36, 92);">&nbsp&nbsp&nbsp&nbsp&nbspOur monitoring and evaluation unit carries out a systematic and objective examination of the<br>
                            relevance, effectiveness, efficiency, and impact of our activities in regards to our specified<br>
                            objectives. The purpose of evaluating projects is to isolate errors in order to avoid them in the<br>
                            future and to underline and promote successful mechanisms for current and prospective projects.<br>
                            Their work allows us to report the progress of implemented activities to our donors and partners.<br><br>
                            Our monitoring and evaluation unit’s continuous monitoring allows us to provide all stakeholders<br>
                            with early detailed information on the progress or delay of ongoing activities. We are thus able to<br>
                            determine whether our program’s work and the efforts put out by our different units and<br>
                            departments are taking place according to plan, so that action can be taken to correct any<br>
                            deficiencies as quickly as possible.<br><br>
                            &nbsp&nbsp&nbsp&nbsp&nbspThis unit ensures everything is running smoothly while reporting errors or delays and taking<br>
                            necessary action to correct them.
                        </div>
                  </div>
                   
                        <div  style="text-align:center;justify-content:center;padding-top:30px ; ">
                        <img class ="line"src="assets/icon/line.svg" ></div>


                        <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
                            <div style="display:inline-block;vertical-align:top;">
                               <img src="assets/icon/heart.svg" alt="img"/>
                               </div>
                               <div style="display:inline-block;font-family:Patrick Hand ;font-size: 30px;color:rgb(21, 137, 158)">
                              
                                   &nbsp&nbspInternal Audit&nbsp&nbsp
                               
                               </div>
                               <div style="display:inline-block;vertical-align:top;">
                                <img src="assets/icon/heart.svg" alt="img"/>
                                </div></div>
                                <div style="text-align: center;">
                                    <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:27px;font-family: sans-serif;font-weight:lighter ;">
                                      <p style="color:rgb(2, 36, 92);">&nbsp&nbsp&nbsp&nbsp&nbspThe role of internal auditing is to provide independent assurance that the organization's risk<br>
                                        management, governance, and internal control processes are operating effectively. Consistent<br>
                                        with their role, evaluators check and analyze budget lines then report findings in their work to<br>
                                        management with recommendations regarding the issues observed. Our audit team is present to<br>
                                        verify that donated funds are being well managed and transparently spent.<br><br><br><br>
                                      </div>
                                </div>
                                
<?php 
include_once('components/Footer.php');
    ?>

        
  </body>
</html>