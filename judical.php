<?php
include_once('components/Header.php');

?>
        <link rel="stylesheet" href="assets/css/whatwedo.css"> 
        <div class="image" style=" background-image: url(assets/icon/whatwedo.png);border-bottom: 35px solid rgb(21, 137, 158); text-align: center;display: block; height: 550px;filter: brightness(100%);min-width: 100%; width: 100%; min-width: 100%;">
            <h1 class="heading2">What We Do</h1>
         </div>
         <!-- body-->
         <div class="cont" style="text-align: center;margin-top: 50px;padding-bottom: 60px;">
         <div style="display:inline-block;vertical-align:top;">
            <img src="assets/icon/sun.svg" alt="img"/>
            </div>
            <div class="prev" style="display:inline-block;font-family:Patrick Hand ;font-size: 70px;color:rgb(21, 137, 158);margin-top:-26px ;">
            
                &nbsp&nbspJudicial Protection &<br>
                &nbsp&nbsp Advocacy Department
           
            </div>
            <div style="display:inline-block;vertical-align:top;">
                <img src="assets/icon/sun.svg" alt="img"/>
                </div></div>
              
                    <div style="text-align: center;">
                      <div style="display: inline-block; text-align: left;margin-right:7vh;font-size:23px;font-family: sans-serif;">
                        <p style="color:rgb(2, 36, 92);">&nbsp&nbsp&nbsp&nbsp&nbsp Our Judicial Protection & Advocacy Department works on:<br><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - The amendments of child protection and alternative care laws in Lebanon, mainly la 422/2002 on<br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp the Protection of Juveniles in Conflict with the Law and/or at Risk.<br><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp - The implementation of a pilot project on family-based alternative care.<br><br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp In 2018, himaya signed a contract with the Ministry of Justice and has become <b>mandated to handle<br>
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp legal protection cases in the North Governorate.<br><br></b>
                           <b>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp In 2021, our contract with the Ministry of Justice expanded to the Bekaa region, and we were<br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp able to widen our reach to the North and Bekaa regions</b> by being present with the child from the<br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp stage of preliminary investigation, until the closure of the case by the juvenile judge. On another<br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp and, our partnership with the Bar Association in Tripoli, also enabled us to secure representation of<br>
                              &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp at-risk children when there is legal action against offenders.<br><br><br>
                        </div>
                  </div>
                   
                  <?php 
include_once('components/Footer.php');
    ?>
 
  </body>
</html>